$(document).ready(function () {

    // colours slider
    $('.colours-slider').slick({
        fade: true,
        asNavFor: '.colours-slider-name',
        arrows: false,
        autoplay: true
    })

    $('.colours-slider-name').slick({
        fade: true,
        asNavFor: '.colours-slider',
        dots: true,
        arrows: false,
    })

    $('.colours-slider').on('beforeChange', function (event, slick,
        currentSlide, nextSlide) {
        if (nextSlide === 2) {
            $(this).closest('.colours').addClass('white')
        } else {
            $(this).closest('.colours').removeClass('white')
        }
    });

    let $window = $(window);

    $(window).scroll(function () {
        $('.video-container video').each(function (index, value) {
            let $video = $(this)
            let $topOfVideo = $video.offset().top;
            let $bottomOfVideo = $video.offset().top + $video.outerHeight();

            let $topOfScreen = $window.scrollTop();
            let $bottomOfScreen = $window.scrollTop() + $window.innerHeight();

            if (($bottomOfScreen > $bottomOfVideo) && ($topOfScreen < $topOfVideo)) {
                $video[0].play();
            } else {
                $video[0].pause();
            }
        });

    });


})